package cn.tedu.sp01.service;

import cn.tedu.sp01.pojo.Item;

import java.util.List;

public interface ItemService {
    //获取商品
    List<Item> getItems(String orderId);
    //减少库存
    void decreaseNumbers(List<Item> list);
}